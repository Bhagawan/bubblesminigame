package com.example.bubblesminigame.util

import android.content.Context

class GameRecords {
    companion object {
        fun saveRecord(context: Context, record: Int) {
            val shP = context.getSharedPreferences("records", Context.MODE_PRIVATE)
            shP.edit().putInt("record", record).apply()
        }

        fun getRecord(context: Context) : Int = context.getSharedPreferences("records", Context.MODE_PRIVATE).getInt("record", 0)
    }
}