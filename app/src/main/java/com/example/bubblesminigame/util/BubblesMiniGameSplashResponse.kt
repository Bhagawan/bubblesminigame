package com.example.bubblesminigame.util

import androidx.annotation.Keep

@Keep
data class BubblesMiniGameSplashResponse(val url : String)