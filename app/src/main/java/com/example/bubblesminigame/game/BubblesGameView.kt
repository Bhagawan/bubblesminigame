package com.example.bubblesminigame.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.bubblesminigame.R
import com.example.bubblesminigame.util.GameRecords
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.*

class BubblesGameView(context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {
    private var mWidth = 0
    private var mHeight = 0

    private var bubbleRadius = 0.0f
    private var bubbles = ArrayList<Bubble>()

    private var activeBubble : Bubble? = null
    private var bubbleDX = 0.0f
    private var bubbleDY = 0.0f

    private var arrowAngle: Float? = null

    private var points = 0

    private val bubblesToDelete = ArrayList<Bubble>()
    private val deleteTime = 60
    private var deleteTimer = deleteTime

    private val bubble = BitmapFactory.decodeResource(context.resources, R.drawable.bubble)
    private val restart = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_replay)?.toBitmap()


    private var state = GAME

    companion object {
        const val GAME = 0
        const val END_POPUP = 1
    }

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            bubbleRadius = mWidth / 14.0f
            restart()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            drawBubbles(it)
            if(state == GAME) {
                updateBubble()
            }
            drawUI(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                when(state) {
                    GAME -> {
                        if(bubbleDX == 0.0f && bubbleDY == 0.0f )updateArrow(event.x, event.y)
                    }
                    END_POPUP -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                when(state) {
                    GAME -> {
                        if(bubbleDX == 0.0f && bubbleDY == 0.0f )updateArrow(event.x, event.y)
                    }
                    END_POPUP -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(state) {
                    GAME -> {
                        if(event.x in 0.0f..(bubbleRadius * 2.0f + 10) && (event.y in 0.0f..(bubbleRadius * 2.0f + 10))) restart()
                        else arrowAngle?.let { trowBubble() }
                    }
                    END_POPUP -> {
                        restart()
                    }
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public

    fun getState() : GameState = GameState(bubbles, activeBubble, bubbleDX, bubbleDY, points)

    fun setState(savedState: GameState) {
        bubbles = ArrayList(savedState.bubbles)
        activeBubble = savedState.activeBubble
        bubbleDX = savedState.dX
        bubbleDY = savedState.dY
        points = savedState.points
        bubblesToDelete.clear()
        deleteTimer = deleteTime
        if(activeBubble == null) activeBubble = Bubble(ColorFilters.values().random(), mWidth / 2.0f, mHeight.toFloat() - bubbleRadius, emptyList())
    }

    //// Private

    private fun drawBubbles(c: Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        for(b in bubbles) {
            p.colorFilter = b.color.colorFilter
            if(b in bubblesToDelete) {
                val newR = bubbleRadius * (deleteTimer.toFloat() / deleteTime.toFloat())
                c.drawBitmap(bubble, null, Rect(
                    (b.x - newR).toInt(),
                    (b.y - newR).toInt(),
                    (b.x + newR).toInt(),
                    (b.y + newR).toInt()), p)
            }
            else c.drawBitmap(bubble, null, Rect(
                (b.x - bubbleRadius).toInt(),
                (b.y - bubbleRadius).toInt(),
                (b.x + bubbleRadius).toInt(),
                (b.y + bubbleRadius).toInt()), p)
        }
        if(deleteTimer <= 10) {
            for(b in bubblesToDelete) bubbles.remove(b)
            points += bubblesToDelete.size * 10
            bubblesToDelete.clear()
            deleteTimer = deleteTime
        }
        if(bubblesToDelete.isNotEmpty()) deleteTimer--
        activeBubble?.let {
            p.colorFilter = it.color.colorFilter
            c.drawBitmap(bubble, null, Rect(
                (it.x - bubbleRadius).toInt(),
                (it.y - bubbleRadius).toInt(),
                (it.x + bubbleRadius).toInt(),
                (it.y + bubbleRadius).toInt()), p)
        }
    }

    private fun drawUI(c: Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.strokeWidth = 3.0f
        p.textSize = 60.0f
        p.textAlign = Paint.Align.CENTER

        when(state) {
            GAME -> {
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey_dark, null)
                c.drawRect(0.0f, 0.0f, mWidth.toFloat(), bubbleRadius * 2.0f, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey_light, null)
                c.drawRect(0.0f, bubbleRadius * 2.0f, mWidth.toFloat(), bubbleRadius * 2.0f + 10, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.gold, null)
                c.drawText(points.toString(), mWidth / 2.0f, bubbleRadius * 1.3f, p)

                p.color = Color.RED
                p.strokeWidth = 2.0f
                c.drawLine(0.0f, mHeight - bubbleRadius * 2, mWidth.toFloat(), mHeight - bubbleRadius * 2, p)

                restart?.let { c.drawBitmap(it, null, Rect(10, 10, (bubbleRadius * 1.8f + 10).toInt(), (bubbleRadius * 1.8f + 10).toInt()), p) }
                arrowAngle?.let {
                    val arrowEndX = mWidth / 2.0f + sin(it) * bubbleRadius * 4
                    val arrowEndY = mHeight - cos(it) * bubbleRadius * 4
                    p.color = Color.WHITE
                    c.drawLine(mWidth / 2.0f, mHeight - bubbleRadius, arrowEndX, arrowEndY, p)

                    val middleX = arrowEndX + (mWidth / 2.0f - arrowEndX) * 0.2f
                    val middleY = arrowEndY + (mHeight - bubbleRadius - arrowEndY) * 0.2f

                    val nX : Float
                    val nY : Float

                    if(arrowEndX - mWidth / 2.0f == 0.0f) {
                        nX = 1.0f
                        nY = 0.0f
                    } else if(arrowEndY - mHeight + bubbleRadius == 0.0f) {
                        nX = 0.0f
                        nY = 1.0f
                    } else {
                        nX = 1.0f
                        nY = -(middleX - arrowEndX) / (middleY - arrowEndY)
                    }
                    val nLen = sqrt(nX.pow(2) + nY.pow(2))
                    val eNX = nX / nLen
                    val eNY = nY / nLen

                    val cat = sqrt(((mWidth / 2.0f - arrowEndX) / 2.0f).pow(2) + ((mHeight - bubbleRadius - arrowEndY) / 2.0f).pow(2))
                    val angle = acos(cat / (bubbleRadius * 2.0f))
                    val cat2 = sin(angle) * 20

                    val finalX = eNX * cat2
                    val finalY = eNY * cat2
                    c.drawLine(arrowEndX, arrowEndY,  middleX + finalX, middleY + finalY, p)
                    c.drawLine(arrowEndX, arrowEndY, middleX - finalX, middleY - finalY, p)
                }
            }
            END_POPUP -> {
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey_dark, null)
                c.drawRoundRect(mWidth * 0.15f, mHeight * 0.3f - 100, mWidth * 0.85f, mHeight * 0.6f, 20f, 20f, p)
                p.style = Paint.Style.STROKE
                p.color = Color.BLUE
                c.drawRoundRect(mWidth * 0.15f, mHeight * 0.3f - 100, mWidth * 0.85f, mHeight * 0.6f, 20f, 20f, p)
                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey_dark, null)
                val pad = (restart?.width ?: 90) * 0.5f + 5
                c.drawRoundRect(mWidth * 0.5f - pad, mHeight * 0.7f - pad, mWidth * 0.5f + pad, mHeight * 0.7f + pad, 20f, 20f, p)

                val record = GameRecords.getRecord(context)
                p.color = if(record > points) Color.WHITE else ResourcesCompat.getColor(context.resources, R.color.gold, null)
                c.drawText("Результат: $points", mWidth * 0.5f, mHeight * 0.3f, p)

                p.color = ResourcesCompat.getColor(context.resources, R.color.gold, null)
                c.drawText("Рекорд: $record", mWidth * 0.5f, mHeight * 0.5f, p)

                p.style = Paint.Style.STROKE
                p.color = Color.WHITE
                c.drawRoundRect(mWidth * 0.5f - pad + 5, mHeight * 0.7f - pad + 5, mWidth * 0.5f + pad - 5, mHeight * 0.7f + pad - 5, 20f, 20f, p)

                restart?.let { c.drawBitmap(restart, (mWidth - restart.width) * 0.5f, mHeight * 0.7f - restart.height * 0.5f, p) }
            }
        }
    }

    private fun updateBubble() {
        activeBubble?.let {
            val near = checkNearBubbles(it.x + bubbleDX, it.y + bubbleDY)
            if(near.isNotEmpty()) {
                bubbleDX = 0.0f
                bubbleDY = 0.0f
                if(near.size == 1) {
                    val vecX = near[0].x - it.x
                    val vecY = near[0].y - it.y
                    val dist = sqrt(vecX.pow(2) + vecY.pow(2))
                    it.x += vecX * ((dist - bubbleRadius * 2.0f) / dist)
                    it.y += vecY * ((dist - bubbleRadius * 2.0f) / dist)
                } else {
                    val firstX = near[near.size - 2].x
                    val firstY = near[near.size - 2].y
                    val secondX = near[near.size - 1].x
                    val secondY = near[near.size - 1].y

                    val middleX = firstX + (secondX - firstX) / 2.0f
                    val middleY = firstY + (secondY - firstY) / 2.0f

                    val nX : Float
                    val nY : Float

                    if(secondX - firstX == 0.0f) {
                        nX = 1.0f
                        nY = 0.0f
                    } else if(secondY - firstY == 0.0f) {
                        nX = 0.0f
                        nY = 1.0f
                    } else {
                        nX = 1.0f
                        nY = -(secondX - firstX) / (secondY - firstY)
                    }
                    val nLen = sqrt(nX.pow(2) + nY.pow(2))
                    val eNX = nX / nLen
                    val eNY = nY / nLen

                    val cat = sqrt(((secondX - firstX) / 2.0f).pow(2) + ((secondY - firstY) / 2.0f).pow(2))
                    val angle = acos(cat / (bubbleRadius * 2.0f))
                    val cat2 = sin(angle) * bubbleRadius * 2

                    var finalX = eNX * cat2
                    var finalY = eNY * cat2

                    if(finalY.sign != (it.y - middleY).sign) {
                        finalY *= -1
                        finalX *= -1
                    }

                    it.x = middleX + finalX
                    it.y = middleY + finalY
                }
                for(b in checkNearBubbles(it.x, it.y)) {
                    val l = sqrt((b.x - it.x).pow(2) + (b.y - it.y).pow(2))
                    if(l < bubbleRadius * 1.9f) {
                        it.x += (it.x - b.x) * ((bubbleRadius * 2.0f - l) / l)
                        it.y += (it.y - b.y) * ((bubbleRadius * 2.0f - l) / l)
                    }
                }

                it.connected = checkNearBubbles(it.x, it.y)
                bubbles.add(it)
                checkHit()
            } else if(it.y <= bubbleRadius * 2 + 10) {
                it.y = bubbleRadius * 3 + 10
                it.connected = checkNearBubbles(it.x, it.y)
                bubbles.add(it)
                activeBubble = null
                bubbleDX = 0.0f
                bubbleDY = 0.0f
            } else {
                if(it.x + bubbleDX !in bubbleRadius..(mWidth - bubbleRadius)) {
                    it.x = if(bubbleDX  < 0 ) bubbleRadius else mWidth - bubbleRadius
                    bubbleDX *= -1
                } else it.x += bubbleDX
                it.y += bubbleDY
            }
        }
        if(activeBubble == null && bubblesToDelete.isEmpty()) activeBubble = Bubble(ColorFilters.values().random(), mWidth / 2.0f, mHeight.toFloat() - bubbleRadius, emptyList())
    }

    private fun updateArrow(pointX: Float, pointY: Float) {
        arrowAngle = if(pointX < mWidth / 2) {
            acos((pointX - mWidth / 2.0f ).absoluteValue / sqrt((pointX - mWidth / 2.0f ).pow(2) + (mHeight - if(pointY < mHeight - bubbleRadius * 2) pointY else mHeight - bubbleRadius * 2).pow(2))) - (PI / 2.0f).toFloat()
        } else {
            (PI / 2.0f).toFloat() - acos((pointX - mWidth / 2 ) / sqrt((pointX - mWidth / 2).pow(2) + (mHeight - if(pointY < mHeight - bubbleRadius * 2) pointY else mHeight - bubbleRadius * 2).pow(2)))
        }
    }

    private fun trowBubble() {
        arrowAngle?.let {
            bubbleDX = sin(it) * bubbleRadius / 1
            bubbleDY = - cos(it) * bubbleRadius / 1
            arrowAngle = null
        }
    }

    private fun generateStartBubbles() {
        for(y in 0..3) {
            for(x in 0..(mWidth / (bubbleRadius * 2)).toInt()) {
                val xx = bubbleRadius + x * bubbleRadius * 2 + if(y % 2 > 0) bubbleRadius else 0.0f
                val yy = bubbleRadius * 3 + 10 + y * bubbleRadius * 1.8f
                val color = ColorFilters.values().random()
                if(xx <= mWidth - bubbleRadius) bubbles.add(Bubble(color, xx, yy, checkNearBubbles(xx, yy).filter { it.color == color }))
            }
        }
    }

    private fun checkNearBubbles(x: Float, y: Float): List<Bubble> {
        val list = ArrayList<Bubble>()
        for(bb in bubbles) {
            val distance = sqrt((x - bb.x).pow(2) + (y - bb.y).pow(2))
            if(distance < bubbleRadius * 2.1f) list.add(bb)
        }
        return list
    }

    private fun checkHit() {
        activeBubble?.let {
            val group = arrayListOf(it)
            getBubbleGroup(it, group)
            if(group.size >= 3) {
                activeBubble = null
                bubblesToDelete.addAll(group)
                bubblesToDelete.addAll(getFloatingBubbles())
            } else {
                if (it.y > mHeight - bubbleRadius * 3) {
                    if(GameRecords.getRecord(context) < points) GameRecords.saveRecord(context, points)
                    state = END_POPUP
                }
                else activeBubble = Bubble(ColorFilters.values().random(), mWidth / 2.0f, mHeight.toFloat() - bubbleRadius, emptyList())
            }
        }
    }

    private fun getBubbleGroup(b: Bubble, group: ArrayList<Bubble>) {
        for(nearBubble in checkNearBubbles(b.x, b.y)) {
            if(b.color == nearBubble.color && !group.contains(nearBubble)) {
                group.add(nearBubble)
                getBubbleGroup(nearBubble, group)
            }
        }
    }

    private fun getFloatingBubbles(): List<Bubble> {
        var list = ArrayList<Bubble>()

        list.addAll(bubbles.filter { (it.y - (bubbleRadius * 3 + 10)).absoluteValue < 10 && !bubblesToDelete.contains(it) })
        for (b in list.toList()) list = getFixedBubbles(b, list)

        return bubbles.filter { !list.contains(it) }
    }

    private fun getFixedBubbles(start: Bubble, group: ArrayList<Bubble>): ArrayList<Bubble> {
        var list = group
        for(b in checkNearBubbles(start.x, start.y)) {
            if(!group.contains(b) && !bubblesToDelete.contains(b)) {
                group.add(b)
                list = getFixedBubbles(b, group)
            }
        }
        return  list
    }

    private fun restart() {
        points = 0
        state = GAME
        bubbles.clear()
        generateStartBubbles()
        bubblesToDelete.clear()
        deleteTimer = deleteTime
        activeBubble = Bubble(ColorFilters.values().random(), mWidth / 2.0f, mHeight.toFloat() - bubbleRadius, emptyList())
    }
}