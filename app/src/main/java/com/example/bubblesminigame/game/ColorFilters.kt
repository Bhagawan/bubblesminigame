package com.example.bubblesminigame.game

import android.graphics.Color
import android.graphics.LightingColorFilter

enum class ColorFilters(val colorFilter: LightingColorFilter) {
    BLUE(LightingColorFilter(Color.BLUE, 1)),
    RED(LightingColorFilter(Color.RED, 1)),
    YELLOW(LightingColorFilter(Color.YELLOW, 1)),
    GREEN(LightingColorFilter(Color.GREEN, 1)),
    MAGENTA(LightingColorFilter(Color.MAGENTA, 1))
}