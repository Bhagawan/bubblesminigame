package com.example.bubblesminigame.game

data class GameState(val bubbles: List<Bubble>, val activeBubble : Bubble?, val dX: Float, val dY: Float, val points: Int)
