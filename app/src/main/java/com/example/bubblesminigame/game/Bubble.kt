package com.example.bubblesminigame.game


data class Bubble(val color: ColorFilters, var x: Float, var y: Float, var connected: List<Bubble> )